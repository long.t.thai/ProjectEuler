package projecteuler

// https://projecteuler.net/problem=1
object MultipliesOf3And5 {

  def getSumOfMultipliesOf3And5(list: List[Int]): Int = {
    return list.filter(num => isMultipliesOf3(num) || isMultipliesOf5(num)).sum
  }

  def isMultipliesOf3(num: Int): Boolean = num % 3 == 0
  def isMultipliesOf5(num: Int): Boolean = num % 5 == 0

  def main(args: Array[String]) = {
    println(getSumOfMultipliesOf3And5(1 to 999 toList))
  }
}
