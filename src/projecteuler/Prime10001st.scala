package projecteuler

import scala.annotation.tailrec

// problem 7
object Prime10001st {
  def isPrime(num: Long): Boolean = {
    for (i <- (2L to Math.sqrt(num).toLong)) {
      if (num % i == 0) {
        return false
      }
    }
    return true
  }

  @tailrec
  def getPrime(currentNum: Long, counter: Int, targetCount: Int): Long = {
    if (!isPrime(currentNum)) {
      return getPrime(currentNum + 2, counter, targetCount)
    } else {
      val newCounter = counter + 1
      if (newCounter == targetCount) {
        return currentNum
      }
      return getPrime(currentNum + 2L, newCounter, targetCount)
    }
  }

  def getPrime(targetCount: Int): Long = targetCount match {
    case 1 => return 2L
    case _ => return getPrime(3, 1, targetCount)
  }

  def main(args: Array[String]): Unit = {
    println(getPrime(10001))
  }
}
