package projecteuler

object SummationOfPrimes {
  def main(args: Array[String]): Unit = {
    val num = 2000000L
//    val primes = LargestPrimeFactor.getPrimes(num)
//    println(primes.sum)

    println((2L to num).filter(Prime10001st.isPrime(_)).sum)

  }
}
