package projecteuler

object SpecialPythaoreanTriplet {

  def doThing: Int = {
    val list = (1 to 998).toList
    list.foreach(a => {
      list.foreach(b => {
        list.foreach(c => {
          if (a*a + b*b == c*c && a + b +c  == 1000) {
            println("%s\t%s\t%s".format(a, b, c))
            return a * b * c
          }
        })
      })
    })
    return -1
  }

  def main(args: Array[String]): Unit = {
    println(doThing)
  }
}
