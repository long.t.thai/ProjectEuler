package projecteuler

// problem 6
object SumSquareDifference {
  def calSumSquareDifference(num: Int): Int = {
    val list = (1 to num).toList
    list.flatMap(v1 => list.map(v2 => (v1, v2)))
      .filter(pair => pair._1 != pair._2)
      .map(pair => pair._1 * pair._2)
      .sum
  }

  def calSumSquareDifferenceBruteForce(num: Int): Int = {
    val list = (1 to num).toList
    return list.sum * list.sum - list.map(num => num * num).sum
  }

  def main(args: Array[String]): Unit = {
    println(calSumSquareDifference(10))
    println(calSumSquareDifference(100))
    println(calSumSquareDifferenceBruteForce(100))
  }
}
