package projecteuler

import scala.annotation.tailrec

// https://projecteuler.net/problem=2
object EvenFibonancciNumbers {

  @tailrec
  def createFibonacciSequence(firstVal: Int, secondVal: Int, currentSequence: List[Int], upperBound: Int): List[Int] = {
    val newVal = firstVal + secondVal
    if (newVal >= upperBound) {
      return currentSequence
    }

    val newSequence = currentSequence :+ newVal
    return createFibonacciSequence(secondVal, newVal, newSequence, upperBound)
  }

  def main(args: Array[String]): Unit = {
    val seq = createFibonacciSequence(1, 2, List(1, 2), 4000000)
    val sum = seq.filter(num => num % 2 == 0).sum
    println(sum)
  }
}
