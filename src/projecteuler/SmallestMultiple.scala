package projecteuler

// problem 5
object SmallestMultiple {

  def calculateSmallestMultiple(primes: List[Long], power: Int, result: Long, upperBound: Long): Long = primes match {
    case Nil => return result
    case _ => {
      val remainingPrimes = primes.filter(p => Math.pow(p, power) <= upperBound)
      println(remainingPrimes)
      val newResult = result * remainingPrimes.product
      println(newResult)
      return calculateSmallestMultiple(remainingPrimes, power + 1, newResult.toLong, upperBound)
    }
  }

  def main(args: Array[String]): Unit = {
    val num = 20L
    val primes = LargestPrimeFactor.getPrimes(num)
//    val nonPrimes = (2L to num).filter(num => !primes.contains(num))
//    println(primes.product)
//    println(primes)
//    println(nonPrimes)

    println(calculateSmallestMultiple(primes, 1, 1, num))
  }
}
