package projecteuler

// https://projecteuler.net/problem=4
object LargestPalindromeProduct {
  def isPalindrome(num: Int): Boolean = {
    val chars = num.toString.toCharArray
    for (i <- 0 to chars.size / 2) {
      if (chars(i) != chars(chars.size - 1 - i)) {
        return false
      }
    }
    return true
  }

  def getAllProducts(l1: List[Int], l2: List[Int]): List[Int] = {
    return l1.flatMap(n1 => {
      l2.map(n2 => {
        n1 * n2
      })
    })
  }

  def getLargestPalindromeProduct: Int = {
    val list = Range(100, 999).toList
    return getAllProducts(list, list).filter(isPalindrome(_)).max
  }

  def main(args: Array[String]): Unit = {
    println(getLargestPalindromeProduct)
  }
}
