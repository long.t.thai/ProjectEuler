package projecteuler

// https://projecteuler.net/problem=3
object LargestPrimeFactor {

  private val STEP = 1000000

  def getLargestPrimeFactorImproved(num: Long): BigInt = {
    val primes = getPrimes(List(2), 10, num / 2).reverse
    for (p <- primes) {
      if (num % p == 0) {
        return p
      }
    }
    return -1
  }

  def getStream(upperBound: Long): Stream[Long] = {

    def getStreamRecursive(current: Long, result: Stream[Long]): Stream[Long] = {
      if (current >= upperBound) {
        return result
      }
      val newResult = result :+ current
      return getStreamRecursive(current + 1, newResult)
    }

    return getStreamRecursive(2L, Stream())
  }

  // quick and dirty way to find prime numbers using Sieve of Eratosthenes
  def getPrimes(upperBound: Long): List[Long] = {
    val possiblePrimes = (2L to upperBound).toList
    possiblePrimes.foldLeft(possiblePrimes)((result, num) => result.filter(num2 => num2 <= num || num2 % num != 0)).toList
  }

  def filterNotMultiplies(list: List[Long], multiplies: List[Long]): List[Long] = {
    multiplies.foldLeft(list)((result, multiply) => result.filter(num => num <= multiply || num % multiply != 0))
  }

  def getPrimes(previousPrimes: List[Long], currentUpperBound: Long, hardUpperBound: Long): List[Long] = {
    println(hardUpperBound - currentUpperBound)
    val upperBound = if (currentUpperBound < hardUpperBound) {
      currentUpperBound
    } else {
      hardUpperBound
    }

    val nextBatch = ((previousPrimes.last + 1) to upperBound).toList

    val nextPrimes = filterNotMultiplies(filterNotMultiplies(nextBatch, previousPrimes), nextBatch)
    val result = previousPrimes ++: nextPrimes

    if (upperBound == hardUpperBound) {
      return result
    }

    return getPrimes(result, currentUpperBound + STEP, hardUpperBound)
  }

  def main(args: Array[String]): Unit = {
//    println(getLargestPrimeFactor("600851475143".toDouble))
//    print(getPrimeNumbers(100))
//    println(getLargestPrimeFactorImproved("600851475143".toLong))
//    println(getPrimes(100L))
    println(getPrimes(List(2), STEP, 600851475143L))

//    val list = (2L to 1000L).toList
//    println(filterNotMultiplies(list, list))
  }
}
